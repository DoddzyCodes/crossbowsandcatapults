﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MenuController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        string axisName = "Player0StartGame";
        bool startGame = Input.GetButtonDown(axisName);

        if(startGame)
        {
            SceneManager.LoadScene("GameScene");
        }
        
        bool exitGame = Input.GetKeyDown(KeyCode.Escape);

        if (exitGame)
        {
            Application.Quit();
        }
    }
}
