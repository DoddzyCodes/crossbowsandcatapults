﻿using UnityEngine;
using System.Collections;
using System;

public class CatapultController : MonoBehaviour {
    public int ControllerID = 0;

    public float FullPowerRotation = 0.0f;
    public float MinPowerRotation = 75.0f;

    public float RotationSpeed = 30.0f;
    public float RotationCap = 60.0f;
    public float FireAngle = 45.0f;
    public float MaxFirePower = 10000.0f;

    public GameObject prefabProjectile;

    public AudioClip CatapultLaunchSound;


    private Transform launchPoint;
    private bool firing = false;
    private float firePower = 0.0f;
    private float fireAccuracy = 0.0f;
    private float holdTime = 0.0f;
    private float prevVAxis = 0.0f;

    private float armRotation = 0.0f;
    private bool currentlyFiring = false;

    private Transform armTransform;



    private AudioSource audioSource;
    // Use this for initialization
    void Start () {
        armTransform =  transform.GetChild(0).FindChild("arm");
        launchPoint  =  armTransform.FindChild("LaunchPoint");

        audioSource = GetComponent<AudioSource>();

        UpdateArm();
    }
	
	// Update is called once per frame
	void Update () {
        //Handle rotation first
        string axisName = "Player" + ControllerID + "RotateHorizontal";
        float fRotateAxis =Input.GetAxis(axisName);
        if (Mathf.Abs(fRotateAxis) > 0.1)
        {
            transform.Rotate(new Vector3(0, 1, 0), fRotateAxis * RotationSpeed * Time.deltaTime);
        }

        //Input.GetAxis
        CheckForFiring();

        Animate();
    }

    void CheckForFiring()
    {

        string axisName = "Player" + ControllerID + "FireVertical";
        float vAxis = Input.GetAxis(axisName);
        axisName = "Player" + ControllerID + "FireHorizontal";
        float xAxis = Input.GetAxis(axisName);

        if (vAxis > 0 && firing == false)
        {
            firing = true;
        }

        if(vAxis == 0 && firing == true)
        {
            StartCoroutine(Fire());
            firing = false;
        }
        else if(firing)
        {
            if(Mathf.Abs(vAxis - prevVAxis) > 0.05f)
            {
                holdTime = 0.0f;
            }
            else
            {
                holdTime += Time.deltaTime;
                if(holdTime > 0.1f)
                {
                    firePower = vAxis;
                    fireAccuracy = xAxis;
                    UpdateArm();
                }
            }

            prevVAxis = vAxis;
        }

    }

    IEnumerator Fire()
    {
        currentlyFiring = true;


        audioSource.PlayOneShot(CatapultLaunchSound);

        float fFireRotation = armTransform.localEulerAngles.x;
        do
        {
            Vector3 angle = armTransform.localEulerAngles;
            angle.x += 300.0f * Time.deltaTime;
            armTransform.localEulerAngles = angle;
            yield return new WaitForEndOfFrame();
        } while (armTransform.localEulerAngles.x < MinPowerRotation);
        
        GameObject projectile = Instantiate(prefabProjectile, launchPoint.position, launchPoint.rotation) as GameObject;
        Rigidbody r = projectile.GetComponent<Rigidbody>();


        float fFiringForce = (fFireRotation - MinPowerRotation) / (FullPowerRotation - MinPowerRotation);
        //Debug.Log(fFireRotation);
        //Debug.Log(fFiringForce); ;
        float fAccuracy = fireAccuracy / 10.0f;
        Vector3 rot = launchPoint.transform.rotation * (new Vector3(fAccuracy, 0, 1).normalized);
        r.AddForce(fFiringForce * MaxFirePower * rot, ForceMode.Impulse);

        currentlyFiring = false;
        firePower = 0.0f;
        UpdateArm();
    }

    private void UpdateArm()
    {
        //Debug.Log(armTransform.localEulerAngles.x);

        float fRange = (FullPowerRotation - MinPowerRotation);
        float fRotation = MinPowerRotation + (fRange * firePower);

        armRotation = fRotation;
    }

    private void Animate()
    {
        AnimateArm();
    }

    private void AnimateArm()
    {
        if (currentlyFiring) return;
        //Quaternion targetRot = Quaternion.AngleAxis(armRotation, new Vector3(1, 0, 0));
        Vector3 angle = armTransform.localEulerAngles;
        angle.x = Mathf.Lerp(angle.x, armRotation, Time.deltaTime);
        armTransform.localEulerAngles = angle;
    }
}
