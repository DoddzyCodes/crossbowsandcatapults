﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class WallPlacerController : MonoBehaviour {
    public int ControllerID = 1;

    public Text BlocksRemainingText;


    public Vector2 MinBounds;
    public Vector2 MaxBounds;

    public int MaxNumberBlocksToPlace = 50;

    public float PanSpeed = 10.0f;
    public float RotatorSpeed = 180.0f;
    public float HeightSpeed = 5.0f;
    public float CameraRotatorSpeed = 180.0f;

    public Camera ControlledCamera;

    public List<GameObject> WallPrefabs;

    private GameObject placerWallInstance;
    private GameObject lastPlacedWall;

    private bool switchedPrefabAlready = false;
    private int currentPrefabIndex = 0;

    private int remainingBlocksToPlace;

	// Use this for initialization
	void Start () {
        remainingBlocksToPlace = MaxNumberBlocksToPlace;
        UpdatePlacementIcon();

       //placerWallInstance = Instantiate(prefabWallprefab, transform.position, transform.rotation) as GameObject;
       //placerWallInstance.transform.parent = transform;
       //Destroy(placerWallInstance.GetComponent<Rigidbody>());
       //DestroyObject(placerWallInstance.transform.GetChild(0).gameObject);

        ControlledCamera.transform.position = transform.GetChild(0).GetChild(0).position;
        ControlledCamera.transform.SetParent(transform.GetChild(0).GetChild(0), false);

        BlocksRemainingText.enabled = true;
    }

    private void UpdatePlacementIcon()
    {
        if (placerWallInstance) DestroyObject(placerWallInstance);

        placerWallInstance = Instantiate(WallPrefabs[currentPrefabIndex], transform.position, transform.rotation) as GameObject;
        DisableAllRigidbodiesAndColliders(placerWallInstance);

        placerWallInstance.transform.parent = transform;
    }

    private void DisableAllRigidbodiesAndColliders(GameObject pInstance)
    {
        Rigidbody r = pInstance.GetComponent<Rigidbody>();
        if (r) Destroy(r);

        BoxCollider c = pInstance.GetComponent<BoxCollider>();
        if (c) c.enabled = false;

        for(int i = 0;i < pInstance.transform.childCount;++i)
        {
            DisableAllRigidbodiesAndColliders(pInstance.transform.GetChild(i).gameObject);
        }
        
    }

    private void TranslatePlacer()
    {
        string axisName = "Player" + ControllerID + "PlacementHorizontal";
        float xAxis = Input.GetAxis(axisName);

        axisName = "Player" + ControllerID + "PlacementVertical";
        float yAxis = Input.GetAxis(axisName);

        axisName = "Player" + ControllerID + "PlacementHeight";
        float vertical = Input.GetAxis(axisName);

        float fYAxisMovement = vertical * HeightSpeed * Time.deltaTime;
        float fXAxisMovement = xAxis * PanSpeed * Time.deltaTime;
        float fZAxisMovement = -yAxis * PanSpeed * Time.deltaTime;

        Vector3 movement = new Vector3();
        Transform cameraRotTransform = transform.GetChild(0);

        Vector3 forward = cameraRotTransform.forward; ;
        Vector3 right = cameraRotTransform.right;

        if(ControllerID == 2)
        {
            forward = -1 * forward;
            right = -1 * right;
        }

        movement += fZAxisMovement * forward;
        movement += fXAxisMovement * right;
        movement.y += fYAxisMovement;

        if ((transform.position.z + movement.z) < MinBounds.y || (transform.position.z + movement.z) > MaxBounds.y)
            movement.z = 0.0f;
       //if ((transform.position.x + movement.z) < MinBounds.y || (transform.position.x + movement.z) < MaxBounds.x)
       //    movement.z = 0.0f;
       //if ((transform.position.x + movement.y) < 0.0f || (transform.position.y + movement.y) < 30.0f)
       //     movement.y = 0.0f;

        transform.Translate(movement, Space.World);
    }

    private void RotatePlacer()
    {
        string axisName = "Player" + ControllerID + "PlacementRotate";
        float xAxis = Input.GetAxis(axisName);

        Quaternion rotator = Quaternion.AngleAxis(xAxis * RotatorSpeed * Time.deltaTime, new Vector3(0, 1, 0));
        placerWallInstance.transform.rotation = rotator * placerWallInstance.transform.rotation;
    }

    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Q)) SnapToGround();

        string axisName = "Player" + ControllerID + "PlacementSwitchPrefab";
        float switchPrefab = Input.GetAxis(axisName);
        if (switchPrefab == -1 && !switchedPrefabAlready)
        {
            switchedPrefabAlready = true;
            currentPrefabIndex = --currentPrefabIndex >= 0 ? currentPrefabIndex : WallPrefabs.Count - 1;
            UpdatePlacementIcon();
        }
        if (switchPrefab == 1 && !switchedPrefabAlready)
        {
            switchedPrefabAlready = true;
            currentPrefabIndex = ++currentPrefabIndex < WallPrefabs.Count ? currentPrefabIndex : 0;
            UpdatePlacementIcon();
        }
        if (switchPrefab == 0) switchedPrefabAlready = false;

        axisName = "Player" + ControllerID + "PlacementSnapToGround";
        bool snap = Input.GetButtonDown(axisName);
        if (snap) SnapToGround();

        TranslatePlacer();
        RotatePlacer();


        axisName = "Player" + ControllerID + "PlacementCameraRotate";
        float xAxis = Input.GetAxis(axisName);

        Quaternion rotator = Quaternion.AngleAxis(xAxis * RotatorSpeed * Time.deltaTime, new Vector3(0, 1, 0));
        transform.GetChild(0).rotation = rotator * transform.GetChild(0).rotation;


        axisName = "Player" + ControllerID + "PlacementAccept";
        bool accept = Input.GetButtonDown(axisName);

        int blockCost = placerWallInstance.transform.childCount;
        if (accept && blockCost <= remainingBlocksToPlace)
        {
            lastPlacedWall = Instantiate(WallPrefabs[currentPrefabIndex], 
                transform.position,
                 placerWallInstance.transform.rotation) as GameObject;

            remainingBlocksToPlace -= blockCost;
            UpdateBlockText();
        }
        else if (accept && blockCost >= remainingBlocksToPlace)
        {
            Debug.Log("Controller " + ControllerID + " can not build any more walls!");
        }

        axisName = "Player" + ControllerID + "PlacementRemoveLast";
        bool removeLast = Input.GetButtonDown(axisName);
        if(removeLast)
        {
            if(lastPlacedWall)
            {
                remainingBlocksToPlace += lastPlacedWall.transform.childCount;
                UpdateBlockText();

                DestroyObject(lastPlacedWall);
                lastPlacedWall = null;
            }          
        }

        ControlledCamera.transform.LookAt(placerWallInstance.transform.position, new Vector3(0, 1, 0));
    }

    private void UpdateBlockText()
    {
        BlocksRemainingText.text = "Blocks Remaining: " + remainingBlocksToPlace;
    }

    public void SnapToGround()
    {
        RaycastHit hit;
        float fHighestPoint = -10000.0f;
        if (Physics.Raycast(transform.position, new Vector3(0, -1, 0), out hit, 30.0f))
        {
            fHighestPoint = hit.point.y;
        }
        if (Physics.Raycast(transform.position + new Vector3(1, 0, 0), new Vector3(0, -1, 0), out hit, 30.0f))
        {
            if(fHighestPoint < hit.point.y) fHighestPoint = hit.point.y;
        }
        if (Physics.Raycast(transform.position - new Vector3(1, 0, 0), new Vector3(0, -1, 0), out hit, 30.0f))
        {
            if (fHighestPoint < hit.point.y) fHighestPoint = hit.point.y;
        }
        if (Physics.Raycast(transform.position + new Vector3(0, 0, 1), new Vector3(0, -1, 0), out hit, 30.0f))
        {
            if (fHighestPoint < hit.point.y) fHighestPoint = hit.point.y;
        }
        if (Physics.Raycast(transform.position - new Vector3(0, 0, 1), new Vector3(0, -1, 0), out hit, 30.0f))
        {
            if (fHighestPoint < hit.point.y) fHighestPoint = hit.point.y;
        }

        if (fHighestPoint < 0.0f) fHighestPoint = 0.0f;

        Vector3 pos = transform.position;
        pos.y = fHighestPoint + 0.1f;
        transform.position = pos;
    }

    public void OnDisablePlacement()
    {
        if (placerWallInstance) DestroyObject(placerWallInstance);
        BlocksRemainingText.enabled = false;
    }

    public bool FinishedPlacingBlocks()
    {
        return remainingBlocksToPlace == 0;
    }

}
