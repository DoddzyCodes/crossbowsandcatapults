﻿using UnityEngine;
using System.Collections;

public class ProjectileController : MonoBehaviour
{

	AudioSource audioSource;
	public AudioClip[] brickCollisionSounds;
	public AudioClip[] groundCollisionSounds;

	void Start ()
	{
        audioSource = GetComponent<AudioSource> ();
	}


	void Update ()
	{
	
	}


	void OnCollisionEnter (Collision other)
	{
		if (other.gameObject.tag == "Brick" ){
            audioSource.PlayOneShot (brickCollisionSounds [Random.Range (0, brickCollisionSounds.Length)], (other.relativeVelocity.magnitude / 15.0f));
		}
			
		if (other.gameObject.tag == "Tower" ){
            audioSource.PlayOneShot (brickCollisionSounds [Random.Range (0, brickCollisionSounds.Length)], (other.relativeVelocity.magnitude / 15.0f));
		}

		if (other.gameObject.tag == "Ground" ){
            audioSource.PlayOneShot (groundCollisionSounds [Random.Range (0, groundCollisionSounds.Length)], (other.relativeVelocity.magnitude / 15.0f));
		}

		//if (collision.relativeVelocity.magnitude > 2.0f)
		//Debug.Log (collision.relativeVelocity.magnitude);
	}

}
