﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class GameController : MonoBehaviour
{
    public Camera PlayerOneCamera;
    public Camera PlayerTwoCamera;

    public CatapultController PlayerOneCatapult;
    public CatapultController PlayerTwoCatapult;

    public WallPlacerController PlayerOnePlacer;
    public WallPlacerController PlayerTwoPlacer;

    public Transform PlayerOneCameraLocation;
    public Transform PlayerTwoCameraLocation;

    public List<TowerState> P1Towers;
    public List<TowerState> P2Towers;

    public GameObject PrefabRedWin;
    public GameObject PrefabBlueWin;

    public Transform P1WinTransform;
    public Transform P2WinTransform;

    public Text GameStartText;

    public Image BuildPhaseImage;
    public Image CatapultPhaseImage;

    public AudioClip BeginCatapultSound;
    public AudioClip BeginGameEndSound;

    private AudioSource audioSource;

    bool bTransitioning = false;

    enum GameState
    {
        PlaceMode,
        CatapultMode,
        GameOver,
    };


    enum Players
    {
        Player1,
        Player2
    };

    private GameState currentState;

    // Use this for initialization
    void Start()
    {
        bTransitioning = false;
        currentState = GameState.PlaceMode;
        GameStartText.enabled = false;

        BuildPhaseImage.enabled = true;
        CatapultPhaseImage.enabled = false;

        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentState == GameState.CatapultMode)
        {
            CheckForGameEnd();
        }
        if (currentState == GameState.PlaceMode)
        {
            CheckForEndPlaceMode();
        }       
    }

    private void CheckForEndPlaceMode()
    {
        if (!bTransitioning && PlayerOnePlacer.FinishedPlacingBlocks() && PlayerTwoPlacer.FinishedPlacingBlocks())
        {
            bTransitioning = true;
            StartCoroutine(SwitchToCatapultMode());
        }
    }

    private void CheckForGameEnd()
    {
        bool p1Remains = false;
        foreach(TowerState state in P1Towers)
        {
            if (state.IsStillStanding()) p1Remains = true;
        }
        bool p2Remains = false;
        foreach (TowerState state in P2Towers)
        {
            if (state.IsStillStanding()) p2Remains = true;
        }

        if(!p1Remains) { EndGame(Players.Player2); }
        if(!p2Remains) { EndGame(Players.Player1); }
    }

    IEnumerator SwitchToCatapultMode()
    {
        GameStartText.enabled = true;

        yield return new WaitForSeconds(2.0f);

        PlayerOneCamera.transform.position = PlayerOneCameraLocation.position;
        PlayerOneCamera.transform.rotation = PlayerOneCameraLocation.rotation;
        PlayerTwoCamera.transform.position = PlayerTwoCameraLocation.position;
        PlayerTwoCamera.transform.rotation = PlayerTwoCameraLocation.rotation;

        PlayerOneCatapult.enabled = true;
        PlayerTwoCatapult.enabled = true;


        PlayerOnePlacer.OnDisablePlacement();
        PlayerTwoPlacer.OnDisablePlacement();

        PlayerOnePlacer.enabled = false;
        PlayerTwoPlacer.enabled = false;

        GameStartText.enabled = false;

        BuildPhaseImage.enabled = false;
        //CatapultPhaseImage.enabled = true;

        audioSource.PlayOneShot(BeginCatapultSound);
        currentState = GameState.CatapultMode;
    }

    private void EndGame(Players winner)
    {
        GameObject winPrefab = null;
        switch(winner)
        {
            case Players.Player1:
                winPrefab = PrefabRedWin;
                break;
            case Players.Player2:
                winPrefab = PrefabBlueWin;
                break;
            default:
                break;
        }

        Instantiate(winPrefab, P1WinTransform.position, P1WinTransform.rotation);
        Instantiate(winPrefab, P2WinTransform.position, P2WinTransform.rotation);

        currentState = GameState.GameOver;

        audioSource.PlayOneShot(BeginGameEndSound);
        StartCoroutine(TransitionBackToMenu());
    }

    private IEnumerator TransitionBackToMenu()
    {
        yield return new WaitForSeconds(5.0f);
        SceneManager.LoadScene("SplashScene");
    }
}

