﻿using UnityEngine;
using System.Collections;

public class TowerState : MonoBehaviour {

    private bool isStanding;

	// Use this for initialization
	void Start () {
        isStanding = true;
	}
	
	// Update is called once per frame
	void Update () {
        float fDot = Vector3.Dot(transform.up, Vector3.up);
        if (fDot < 0.4f)
            isStanding = false;
	}

    public bool IsStillStanding() { return isStanding; }
}
